instant (2017.2.0.0-3) unstable; urgency=medium

  * Depends: python3-pkg-resources. Closes: #896318, #896363.
  * Standards-Version: 4.1.4

 -- Drew Parsons <dparsons@debian.org>  Sun, 22 Apr 2018 12:05:32 +0800

instant (2017.2.0.0-2) unstable; urgency=medium

  * debian/control: update VCS tags to salsa.debian.org

 -- Drew Parsons <dparsons@debian.org>  Wed, 21 Feb 2018 22:27:21 +0800

instant (2017.2.0.0-1exp1) experimental; urgency=medium

  * New upstream release.
    - this is the official 2017.2.0 release. The previous version was
      a mislabelled beta release.
  * Standards-Version: 4.1.3
  * debhelper compatibility level 11
  * debian/control Priority: optional not extra

 -- Drew Parsons <dparsons@debian.org>  Sat, 20 Jan 2018 00:10:50 +0800

instant (2017.2.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream release.
    - updated debian/upstream/signing-key.asc for signature
      from the FEniCS Project Steering Council (key BED06106DD22BAB3)
  * Standards-Version: 4.1.1

 -- Drew Parsons <dparsons@debian.org>  Sat, 07 Oct 2017 16:03:59 +0800

instant (2017.1.0-2) unstable; urgency=medium

  * Team upload.
  * Standards-Version: 4.1.0
  * debhelper compatibility level 10
  * Remove debian/patches/python-cmake-fix-for-dolfin.patch
    (now applied upstream)

 -- Drew Parsons <dparsons@debian.org>  Sat, 09 Sep 2017 14:28:56 +0800

instant (2017.1.0-1) experimental; urgency=medium

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Thu, 06 Jul 2017 12:44:58 +0200

instant (2016.2.0-3) unstable; urgency=medium

  * d/patches/python-cmake-fix-for-dolfin.patch: Add patch to fix CMake
    file that Instant generates for DOLFIN. This is needed for Python 2
    and Python 3 side-by-side installations.

 -- Johannes Ring <johannr@simula.no>  Tue, 06 Jun 2017 17:55:04 +0200

instant (2016.2.0-2) unstable; urgency=medium

  * Team upload.
  
  [ Johannes Ring ]
  * Support Python 3.
  * d/control: Add python-six to Depends.

 -- Drew Parsons <dparsons@debian.org>  Thu, 19 Jan 2017 22:09:32 +0800

instant (2016.2.0-1) unstable; urgency=medium

  * New upstream release.
  * d/watch: Check pgp signature.
  * d/control: Remove Christophe Prud'homme from Uploaders (closes: #835007).

 -- Johannes Ring <johannr@simula.no>  Thu, 01 Dec 2016 14:02:17 +0100

instant (2016.1.0-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Fri, 24 Jun 2016 13:16:14 +0200

instant (1.6.0-2) unstable; urgency=medium

  * Team upload.
  * debian/control:
      + Update VCS fields after the move to Git.
  * Standards-Version: 3.9.8

 -- Drew Parsons <dparsons@debian.org>  Thu, 28 Apr 2016 18:54:59 +0800

instant (1.6.0-1) unstable; urgency=medium

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Tue, 11 Aug 2015 10:39:35 +0200

instant (1.5.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes needed).
    - Bump X-Python-Version to >= 2.7.
    - Bump python-all in Build-Depends to >= 2.7.

 -- Johannes Ring <johannr@simula.no>  Mon, 12 Jan 2015 20:54:05 +0100

instant (1.4.0-1) unstable; urgency=medium

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.5 (no changes needed).
    - Replace "swig | swig2.0" with "swig" in Depends.

 -- Johannes Ring <johannr@simula.no>  Tue, 03 Jun 2014 12:44:29 +0200

instant (1.3.0-1) unstable; urgency=low

  * New upstream release.
  * debian/watch: Update URL for move to Bitbucket.

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Jun 2013 13:02:30 +0200

instant (1.2.0-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Bump Standards-Version to 3.9.4.
    - Remove DM-Upload-Allowed field.
    - Bump required debhelper version in Build-Depends.
    - Remove cdbs from Build-Depends.
    - Use canonical URIs for Vcs-* fields.
    - Add cmake to Depends field.
  * debian/compat: Bump to compatibility level 9.
  * debian/rules: Rewrite for debhelper (drop cdbs).

 -- Johannes Ring <johannr@simula.no>  Wed, 26 Jun 2013 12:40:27 +0200

instant (1.1.0-1) UNRELEASED; urgency=low

  * New upstream release.
  * debian/watch: Replace http with https in URL.

 -- Johannes Ring <johannr@simula.no>  Thu, 10 Jan 2013	10:04:35 +0100

instant (1.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Wed, 07 Dec 2011 15:06:03 +0100

instant (1.0-beta2-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Fri, 04 Nov 2011 08:44:02 +0100

instant (1.0-beta-1) unstable; urgency=low

  * New upstream release.

 -- Johannes Ring <johannr@simula.no>  Mon, 15 Aug 2011 17:05:53 +0200

instant (0.9.10-1) unstable; urgency=low

  * New upstream release.
  * Move from python-central to dh_python2 (closes: #616849).
    - Remove python-central from Build-Depends.
    - Bump minimum required python-all package version to 2.6.6-3~.
    - Remove XB-Python-Version line.
    - Bump minimum required cdbs version to 0.4.90~.
    - Remove DEB_PYTHON_SYSTEM=pycentral from debian/rules.
    - Replace XS-Python-Version with X-Python-Version.
  * Remove old fields Conflicts, Provides, and Replaces from
    debian/control.
  * Add swig2.0 to Depends field as an alternative to swig.
  * Bump Standards-Version to 3.9.2 (no changes needed).
  * debian/rules: Remove deprecated variable DEB_COMPRESS_EXCLUDE.

 -- Johannes Ring <johannr@simula.no>  Tue, 17 May 2011 13:49:09 +0200

instant (0.9.9-1) unstable; urgency=low

  * New upstream release.
  * Remove python-numeric and python-numarray from suggested packages.
  * Bump Standards-Version to 3.9.1.
  * Remove old fields Conflicts, Provides, and Replaces from
    debian/control.
  * Switch to dpkg-source 3.0 (quilt) format.
  * Package moved from pkg-scicomp to Debian Science.
  * Update Homepage field in debian/control.

 -- Johannes Ring <johannr@simula.no>  Wed, 23 Feb 2011 23:12:35 +0100

instant (0.9.8-1) unstable; urgency=low

  * New upstream release.
  * debian/control:
    - Add DM-Upload-Allowed: yes.
    - Bump debhelper version to 7.
    - Bump Standards-Version to 3.8.4 (no changes needed).
  * debian/compat: Bump to compat level 7.
  * debian/watch: Update download URL.
  * debian/copyright:
    - Add note that Instant is now dual-licensed.
    - Add manpages to list of exceptions (thanks to Mike O'Connor).
    - Update download URL.
    - Add missing upstream author Ilmar Wilbers.

 -- Johannes Ring <johannr@simula.no>  Tue, 16 Feb 2010 10:06:32 +0100

instant (0.9.6-1) unstable; urgency=low

  * Initial release (Closes: #503079)

 -- Johannes Ring <johannr@simula.no>  Mon, 15 Sep 2008 11:19:12 +0200

